#!/bin/bash -eu

DIR=$(mktemp -d)
trap "rm -rf \${DIR}" exit
TARGET_DIR=$(realpath "${PWD}")
pushd "${DIR}"

git clone https://github.com/coconut-svsm/svsm.git --single-branch --depth=1
mv svsm coconut-svsm
pushd coconut-svsm
COMMIT=$(git rev-parse --short HEAD)
DATE=$(git log --date="format:%Y%m%d" --format="%ad" -n 1 ${COMMIT})
mkdir -p external-crates
pushd external-crates
git clone https://github.com/coconut-svsm/packit.git --single-branch --depth=1
popd
popd
tar -c --exclude ".git*" coconut-svsm | xz -c -9 -T0 > ../coconut-svsm-$COMMIT.tar.xz
cp ../coconut-svsm-$COMMIT.tar.xz "${TARGET_DIR}"
popd

sed -i s/%define\ date\.\*/%define\ date\ ${DATE}/ ./coconut-svsm.spec
sed -i s/%define\ commit\.\*/%define\ commit\ ${COMMIT}/ ./coconut-svsm.spec

