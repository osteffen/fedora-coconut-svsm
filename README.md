Fedora Dist-Git for Coconut-SVSM
================================

This is a Fedora dist-git repository for the Coconut SVSM.

Upstream: https://github.com/coconut-svsm/svsm

Fedora Copr: https://copr.fedorainfracloud.org/coprs/g/virtmaint-sig/sev-snp-coconut/

The resulting package provides the SVSM binary under `/usr/share/coconut-svsm/cocnut-svsm.bin`.
It is to be used with an AMD SEV-SNP guest. Specific versions of the host kernel, Qemu, and OVMF are
required (see upstream readme for details.) These are also available in the Copr mentioned above.

Coconut now builds with a stable Rust toolchain, which is available in Fedora 40.
This package builds without network access, and uses only dependencies (crates) shipped in Fedora.

The gdb support of Coconut is disabled in this build, because the required gdbstub crate is not available in Fedora.

Build instructions:
```
git clone https://gitlab.com/osteffen/fedora-coconut-svsm
cd fedora-coconut-svsm
fedpkg --release f40 mockbuild
```
