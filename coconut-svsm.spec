%define date 20231016
%define commit dde36a0
Name: coconut-svsm
Version: 0^%{date}g%{commit}
Release: %autorelease
Summary: The Coconut Secure VM Service Module for AMD SEV-SNP

License: MIT
URL: https://github.com/coconut-svsm/coconut
Source0: file://./coconut-svsm-%{commit}.tar.xz

Patch0001: 0001-override.patch
Patch0002: 0002-log-version.patch
Patch0003: 0003-remove-gdb.patch

BuildRequires: binutils
BuildRequires: curl
BuildRequires: gcc
BuildRequires: git
BuildRequires: make
BuildRequires: rust-bitflags-devel
BuildRequires: rust-log-devel
BuildRequires: rust-proc-macro2-devel
BuildRequires: rust-quote-devel
BuildRequires: rust-std-static-x86_64-unknown-none
BuildRequires: rust-syn-devel
BuildRequires: rust-unicode-ident-devel
BuildRequires: rust-zerocopy-devel
BuildRequires: rust-intrusive-collections-devel
BuildRequires: rust
BuildRequires: cargo

%description
Secure Virtual machine Service Module for use in
confidenial VMs based on AMD SEV-SNP memory encryption.
This is the Coconut SVSM implementation.

%global debug_package %{nil}

%prep
%autosetup -v -n coconut-svsm -D -p1

rm Cargo.lock

# redirect cargo to local crates rom Fedora RPMs
mkdir -p ~/.cargo
cat << EOF > ~/.cargo/config
[source.local-registry]
directory = "/usr/share/cargo/registry"

[source.crates-io]
registry = "https://crates.io"
replace-with = "local-registry"
EOF

%build

# Default flags interfere with svsm build
unset CFLAGS
unset CCFLAGS
unset LDFLAGS
unset RUSTFLAGS

make FEATURES="enable-stacktrace"

%install
mkdir -p %{buildroot}%{_datadir}/%{name}
install \
  -m 444 \
  svsm.bin \
  %{buildroot}%{_datadir}/%{name}/coconut-svsm.bin


%files
%dir %{_datadir}/%{name}/
%{_datadir}/%{name}/coconut-svsm.bin


%changelog
* Tue Oct 10 2023 Oliver Steffen <osteffen@redhat.com> - 0.0.4
- Update to upstream commit c1aa81b
- Use only rust components provided by Fedora. Include the packit
  crate in the source tarball. Patch out gdb support, since gdbstub
  is not available in Fedora.

* Mon Sep 25 2023 Oliver Steffen <osteffen@redhat.com> - 0.0.3
- Update to upstream commit b8a5474

* Wed Sep 13 2023 Oliver Steffen <osteffen@redhat.com> - 0.0.2
- Update to upstream commit 9b7d797

* Wed Aug 9 2023 Oliver Steffen <osteffen@redhat.com> - 0.0.1
- Initial version of the package

